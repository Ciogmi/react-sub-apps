export default function getFizzConfig(navLinkText = 'Go to Foo', path = '/foo') {
    return {
        navLinkText,
        path,
        loader: () => import('./fizz.component')
    }
}