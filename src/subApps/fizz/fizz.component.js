import React from 'react';
import { NavLink, Route } from 'react-router-dom'

import Buzz from './buzz.component';

const Fizz = ({ match: { path } }) => (
  <div>
    <h3>Fizz</h3>
    <NavLink to={`${path}/buzz`}>to buzz</NavLink>
    <Route path={`${path}/buzz`} component={Buzz}/>
  </div>
);

export default Fizz;