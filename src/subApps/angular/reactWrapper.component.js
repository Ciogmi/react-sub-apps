import React, { Component } from 'react';

class ReactWrapper extends Component {

  componentDidMount() {
    // after the root component is imported the angular app needs to be bootstrapped
    import('./testApp/dist/main.bundle').then((bootstrapApp) => bootstrapApp.default());
  }

  componentWillUpdate() {
    // any re-render needs a nea bootstrap of the angular app
    import('./testApp/dist/main.bundle').then((bootstrapApp) => bootstrapApp.default());
  }

  render() {
    const html = "<app-root>Loading</app-root>";

    return (
      <div dangerouslySetInnerHTML={{__html: html}}/>
    )
  }
}

export default ReactWrapper;