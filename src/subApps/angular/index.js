import ReactWrapper from './reactWrapper.component';

export default function getVanillaConfig(navLinkText = 'Go to angular 2 app', path = '/angular') {
  return {
    navLinkText,
    path,
    loader: () => Promise.resolve(ReactWrapper)
  }
}