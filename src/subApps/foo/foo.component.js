import React from 'react';
import { NavLink, Route } from 'react-router-dom'

import Bar from './bar.component';

const Foo = ({ match: { path } }) => (
  <div>
    <h3>Foo</h3>
    <NavLink to={`${path}/bar`}>to bar</NavLink>
    <Route path={`${path}/bar`} component={Bar}/>
  </div>
);

export default Foo;