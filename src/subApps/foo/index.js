export default function getFooConfig(navLinkText = 'Go to Fizz', path = '/fizz') {
    return {
        navLinkText,
        path,
        loader: () => import('./foo.component')
    }
}