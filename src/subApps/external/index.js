export default function getExternalConfig(navLinkText = 'Go to Google') {
    return {
        navLinkText,
        path: 'https://www.google.com'
    }
}