import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';

import MainNavService from './mainNavigation/mainNavigation.service';
import MainNavigation from './mainNavigation/mainNavigation.component';

import getFooConfig from './subApps/foo';
import getFizzConfig from './subApps/fizz';
import getExternalConfig from './subApps/external';
import getVanillaConfig from './subApps/angular';

MainNavService.registerRoutes(
  getFooConfig(),
  getFizzConfig(),
  getExternalConfig(),
  getVanillaConfig()
);

class App extends Component {
  render() {
    const routes = MainNavService.routes;

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <MainNavigation routes={routes}/>
      </div>
    );
  }
}

export default App;
