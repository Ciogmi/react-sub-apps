import React, { Component } from 'react';
import { NavLink, Route, Switch } from 'react-router-dom'

import Bundle from '../bundle-component/bundle.component';

export const navTypes = {
  subApp: {
    type: 'subApp'
  },
  hardLink: {
    type: 'hardLink'
  }
};

class MainNavigation extends Component {
  constructor() {
    super();
    this.navTypes = {...navTypes};
    this.navTypes.subApp.routeRenderer = this.renderSubAppRoute;
    this.navTypes.subApp.linkRenderer = this.renderSubAppLink;
    this.navTypes.hardLink.routeRenderer = this.renderHardLinkRoute;
    this.navTypes.hardLink.linkRenderer = this.renderHardLink;
  }


  render() {
    const { routes } = this.props;

    return (
      <div>
        { this.renderNavLinks(routes) }
        { this.renderRoutes(routes) }
      </div>
    );
  }

  renderRoutes(routes) {
    return (
      <Switch>
        { routes.map(route => this.navTypes[route.type].routeRenderer(route)) }
      </Switch>
    )
  }

  renderNavLinks(routes) {
    return (
      <div className="App-intro">
          { routes.map(route => this.navTypes[route.type].linkRenderer(route)) }
      </div>
    );
  }

  renderSubAppRoute(route) {
    const {
      loader,
      path
    } = route;
    return (
      <Route key={path} path={path} render={(props) => (
          <Bundle load={loader}>
            {(SubApp) => SubApp ? <SubApp {...props}/> : 'loading'}
          </Bundle>
        )}
      />
    );
  }

  renderHardLinkRoute() {
    return null;
  }

  renderSubAppLink(route) {
    const {
      navLinkText,
      path
    } = route;

    return <NavLink className="link" key={path} to={path}>{navLinkText}</NavLink>;
  }

  renderHardLink(route) {
    const {
        navLinkText,
        path
    } = route;

    return (
      <a className="link" key={path} href={path} target="_blank">{navLinkText}</a>
    );
  }
}

export default MainNavigation;