import { navTypes } from './mainNavigation.component';

class MainNavService {
  routes = [];

  registerRoutes(...routeConfigs) {
    this.routes.push(
        ...routeConfigs.map(this.configureRoute)
    );
  }

  configureRoute(config) {
    const {
      subApp: { type: subApp },
        hardLink: { type: hardLink}
    }  = navTypes;

    return {
        ...config,
        type: config.loader ? subApp : hardLink
    }
  }
}

export default new MainNavService();